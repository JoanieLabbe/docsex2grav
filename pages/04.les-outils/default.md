---
title: 'Les Outils'
media_order: 'aircrack-ng-new-logo.jpg,Kismet-logo.png,Metasploit-logo.png,ettercap-english-1.jpg'
---

!!! Cette page a pour but de vous informer de l'utilisté de base des outils de bases de Kali Linux, pour plus d'informations vous pouvez aller visiter les liens disponibles dans les sources et compléter avec vos propres recherches.


## ![](aircrack-ng-new-logo.jpg)
C'est une suite d'outils qui permettent de monitorer, d'attaquer, de tester et de craquer la sécurité de réseaux Wi-Fi.
[https://www.aircrack-ng.org/](https://www.aircrack-ng.org/)
## Armitage
C'est un outil pour Metasploit qui permet de faciliter le travail d'équipe des [équipes rouges](https://en.wikipedia.org/wiki/Red_team).
[http://www.fastandeasyhacking.com/](http://www.fastandeasyhacking.com/)
## Burp suite
La suite Burp contient divers outils qui permettent de sécuriser et de tester la sécurité d'application web.
[https://portswigger.net/burp](https://portswigger.net/burp)

## ![](ettercap-english-1.jpg)
C'est une suite pour les gens dans le milieu de l'attaque qui peut entre autre d'observer les connections actives.
[https://www.ettercap-project.org/](https://www.ettercap-project.org/)
## John the Ripper
C'est un "cracker" de mot de passe.
[https://www.openwall.com/john/](https://www.openwall.com/john/)

## ![](Kismet-logo.png)
C'est un outil qui permet de détexter des réseaux sans fils, des appareils connecter, c'est un observateur des connections actives et plusieurs autres choses par rapport au réseaux sans-fils.
[https://www.kismetwireless.net/](https://www.kismetwireless.net/)
## Maltego
c'est un outil d'exploitation de données qui permet de trouver des relations entre différentes informations.
[https://www.paterva.com/buy/maltego-clients/maltego-ce.php](https://www.paterva.com/buy/maltego-clients/maltego-ce.php)

## ![](Metasploit-logo.png)
C'est un outil de test de pénétration.
[https://www.metasploit.com/](https://www.metasploit.com/)
## Social engineering tools
C'est un outil de test de pénétration construit autour de l'ingénérie sociale.
[https://www.trustedsec.com/tools/the-social-engineer-toolkit-set/](https://www.trustedsec.com/tools/the-social-engineer-toolkit-set/)
## Sqlmap
C'est encore un outil de test de pénétration, mais axé afin de protéger des bases de données SQL.
[http://sqlmap.org/](http://sqlmap.org/)
## Hydra / thc-hydra
C,est un outil qui permet de forcer son authentification a distance.
[https://www.securiteinfo.com/attaques/hacking/outils/thc-hydra.shtml](https://www.securiteinfo.com/attaques/hacking/outils/thc-hydra.shtml)
## Binwalk
C'est un outil d'analyse, d'ingénérie sociale et d'extration d'image de micrologiciel.
[https://github.com/ReFirmLabs/binwalk](https://github.com/ReFirmLabs/binwalk)
## Foremost
C'est une application console de récupération de fichiers.
[http://foremost.sourceforge.net/](http://foremost.sourceforge.net/)
## Volatility
C'est un outil de la mémoire "active" dont la RAM.
[https://www.volatilityfoundation.org/](https://www.volatilityfoundation.org/)
## Sources
* [https://fr.wikipedia.org/wiki/Burp_suite](https://fr.wikipedia.org/wiki/Burp_suite)
### Images
* [https://www.aircrack-ng.org/](https://www.aircrack-ng.org/)
* [https://pentestmag.com/ettercap-tutorial-for-windows/](https://pentestmag.com/ettercap-tutorial-for-windows/)
* [https://fr.wikipedia.org/wiki/Kismet_(logiciel)](https://fr.wikipedia.org/wiki/Kismet_(logiciel))
* [https://fr.wikipedia.org/wiki/Metasploit](https://fr.wikipedia.org/wiki/Metasploit)