---
title: 'KALI LINUX'
media_order: 'kali.png,kali-screenshot.jpg'
taxonomy:
    category:
        - docs
visible: true
---

 ![](kali.png)
!!! La documentation qui suit à pour but de vous informer sur la distribution Kali Linux et de vous expliquer le fonctionnement de plusieurs de ces outils.
## C'est quoi Kali Linux?
* Kali Linux est une distribution **GNU/Linux** basée sur [Debian](https://fr.wikipedia.org/wiki/Debian), comme [Ubuntu](https://fr.wikipedia.org/wiki/Ubuntu_(syst%C3%A8me_d%27exploitation)). Il est le successeur de [BackTrack](https://fr.wikipedia.org/wiki/BackTrack), un projet abandonné dont la dernière version est sortie en août 2012.
* Kali Linux est la fruit du travail de la compagnie **Offensive Security** et des développeurs Mati Aharoni, Devon Kearns et Raphaël Hertzog.
* Le but d'Offensive Security en créant le projet Kali Linux est de **regrouper tous les [outils](../les-outils) nescéssaires aux tests de sécurité informatique**, notamment tout ce qui est test d'intrusion et test de réseau, dans une distribution.
* La première distribution de Kali Linux est sortie le **13 mars 2013**, soit un peu plus d'un an après l'abandon de BackTrack. Depuis la version **2016.2** sortie en août 2016, le système d'exploitation est disponible pré-installée avec de nombreux **environnements graphiques** pouvant être choisi au moment du téléchargement. On y retrouve : [GNOME](https://fr.wikipedia.org/wiki/GNOME), [KDE](https://fr.wikipedia.org/wiki/KDE), [LXDE](https://fr.wikipedia.org/wiki/LXDE), [MATE](https://fr.wikipedia.org/wiki/MATE), [Enlightenment](https://fr.wikipedia.org/wiki/Enlightenment_(logiciel)) et [Xfce](https://fr.wikipedia.org/wiki/Xfce)

![](kali-screenshot.jpg)
# Installation
## Choisir l'image
| Nom de l'image | Taille | Particularité |
| --------------------- | ------ | -----------------|
| Kali Linux 32 bit | 2.9G | La version "Normale" contient un environnement GNOME et la selection d'outil par défaut de Kali-Linux |
| Kali Linux 64-Bit | 2.9G | C'est la version faite pour un processeur plus récent de Kali Linux 64-Bit |
| Kali Linux Large 64-Bit | 3.5G | La version Large contient tout ce que la version "Normale" contient plus quelques outils additionnel |
| Kali Linux Light ARMhf | 803M | C'est la version Light qui est faite pour  un "arm chipset" comme un Rasberry Pi |
| Kali Linux Light 64-Bit | 1.1G | La version Light contient un environnement en Xfce et une petite sélection des outils |
| Kali Linux Light 32-Bit | 1.1G| C'est Kali Linux 32-Bit, mais avec l'environnement graphique LXDE |
| Kali Linux LXDE 64-Bit | 2.7G | C'est Kali Linux 64-Bit, mais avec l'environnement graphique LXDE |
| Kali Linux MATE 64-Bit | 2.8G | C'est Kali Linux 64-Bit, mais avec l'environnement graphique MATE |
| Kali Linux E17 64-Bit | 2.7G | C'est Kali Linux 64-Bit, mais avec l'environnement graphique E17 |
| Kali Linux KDE 64-Bit | 3.2G | C'est Kali Linux 64-Bit, mais avec l'environnement graphique KDE |
| Kali Linux XFCE 64-Bit | 2.7G | C'est Kali Linux 64-Bit, mais avec l'environnement graphique XFCE |
De plus, il existe des versions 32-Bit et 64-Bit spécialement conçues pour l'utilisation de VMware et de VirtualBox!

Pour voir la différence entre les différents environements graphiques, vous pouvez allez voir sur ce [site](https://miloserdov.org/?p=2366).
## Installer la distribution
Vous pouvez suivre le tutoriel suivant pour faire votre installation
! N'oubliez pas votre mot de passe de root!!!
[plugin:youtube](https://www.youtube.com/watch?v=xVjZ8_zZv2k)

### Sources
* [https://fr.wikipedia.org/wiki/Kali_Linux](https://fr.wikipedia.org/wiki/Kali_Linux)
* [https://fr.wikipedia.org/wiki/BackTrack](https://fr.wikipedia.org/wiki/BackTrack)
* [https://www.kali.org/](https://www.kali.org/)
* [https://unix.stackexchange.com/questions/538797/whats-the-difference-between-kali-linux-large-light-and-normal](https://unix.stackexchange.com/questions/538797/whats-the-difference-between-kali-linux-large-light-and-normal)
* [https://unix.stackexchange.com/questions/466649/whats-differant-between-kali-linux-versions](https://unix.stackexchange.com/questions/466649/whats-differant-between-kali-linux-versions)
* [https://www.kali.org/downloads/](https://www.kali.org/downloads/)
#### Logo
* [https://fr.wikipedia.org/wiki/Fichier:Kali-2.0-website-logo-300x90.png](https://fr.wikipedia.org/wiki/Fichier:Kali-2.0-website-logo-300x90.png)